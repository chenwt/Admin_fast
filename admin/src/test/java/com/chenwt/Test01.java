package com.chenwt;

import com.chenwt.modules.system.domain.Role;
import com.chenwt.modules.system.service.RoleService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @class：Test01
 * @campany：zkzj
 * @author:feiniaoying
 * @date:2019-05-04 21:20
 * @description:
 */
public class Test01 extends BaseTest {
    @Autowired
    private RoleService roleService;

    @Test
    public void test01(){
        Role tt = roleService.getById(1L);
        System.out.println("11111"+tt);
        System.out.println(tt.toString());
    }
}

/**
 * @author chenwt
 * 封装ajax
 */
(function () {
    var $ax = function (url, success, error) {
        this.url = url;
        this.type = "post";
        this.data = {};
        this.dataType = "json";
        this.async = false;
        this.success = success;
        this.error = error;
        this.contentType = "application/x-www-form-urlencoded; charset=UTF-8"

    };

    $ax.prototype = {
        start: function () {
            var me = this;

            $.ajax({
                type: this.type,
                url: this.url,
                dataType: this.dataType,
                async: this.async,
                data: this.data,
                timeout: 30000,
                contentType: this.contentType,
                beforeSend: function (data) {

                },
                success: function (data) {
                    me.success(data);
                },
                error: function (data) {
                    me.error(data);
                }
            });
        },

        set: function (key, value) {
            if (typeof key == "object") {
                for (var i in key) {
                    if (typeof i == "function")
                        continue;
                    this.data[i] = key[i];
                }
            } else {
                this.data[key] = (typeof value == "undefined") ? $("#" + key).val() : value;
            }
            return this;
        },

        setData: function (data) {
            this.data = data;
            return this;
        },

        clear: function () {
            this.data = {};
            return this;
        },

        setDataJsonType: function () {
            this.contentType = "application/json";
        }
    };

    window.$ax = $ax;
}());

/**
 * 使用方法
 */
// var ajax = new $ax(url, function (data) {
//     //数据请求成功,base64展示
//     Poster.showPoster(index,data.data);
// }, function (data) {
//     layer.msg("访问超时或请求错误，请刷新页面!", {icon: 5});
// });
// ajax.set("customerId", customerId);
// ajax.set("picture", picture);
// ajax.start();